package pl.cm.springzadanie.model;

import javax.persistence.*;

/**
 * Orders class for Spring Application
 * @author piotr formela
 */

@Entity
@Table(name = "orders")
public class OrderDetails {

    @Id
    @GeneratedValue
    @Column
    private Long id;

    @Column
    private String items;

    @Column
    private String creation_date;

    @Column
    private Long user_id;

    public OrderDetails() {
    }

    public OrderDetails(Long id, String items, String creation_date, Long user_id) {
        this.id = id;
        this.items = items;
        this.creation_date = creation_date;
        this.user_id = user_id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    @Override
    public String toString() {
        return "OrderDetails{" +
                "id=" + id +
                ", items='" + items + '\'' +
                ", creation_date='" + creation_date + '\'' +
                ", user_id=" + user_id +
                '}';
    }
}
