package pl.cm.springzadanie.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.cm.springzadanie.data.OrderRepository;
import pl.cm.springzadanie.model.OrderDetails;
import javax.validation.Valid;
import java.util.Map;

@Controller
//@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderRepository orderRepository;

    @GetMapping("/order")
    public String showList(Map<String, Object> model) {
    model.put("orders", orderRepository.findAll());
    return "order/list";
    }

    @GetMapping("order/add")
    public String showAdd(Map<String, Object> model) {
    model.put("orders", new OrderDetails());
    return "order/add";
    }

    @PostMapping("order/add")
    public String add(@Valid @ModelAttribute("order") OrderDetails order) {
        orderRepository.save(order);
        return "redirect:/order";
    }

    @GetMapping("order/delete/{id}")
    public String delete(@PathVariable("id") Long id) {
        orderRepository.delete(id);
        return "redirect:/order";
    }

    @GetMapping("order/edit/{id}")
    public String edit(@PathVariable("id") Long id, Model model) {
        model.addAttribute("orders", orderRepository.findById(id));
        return "order/edit";
    }

    @PostMapping(value="order/edit/{id}")
    public String save(@Valid @ModelAttribute("order") OrderDetails order){
        orderRepository.save(order);
        return "redirect:/order";
    }

}
