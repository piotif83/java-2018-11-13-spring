package pl.cm.springzadanie.model;

public enum UserRole {

    USER,
    ADMIN
}
