package pl.cm.springzadanie.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import pl.cm.springzadanie.model.UserRole;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/css/**", "/order/**").permitAll()
                .antMatchers("/user/**").hasRole(UserRole.USER.name())
                .antMatchers("/admin/**").hasRole(UserRole.ADMIN.name())
                .and().csrf().disable()
                .httpBasic().and().logout().and().formLogin().loginPage("/login");
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .inMemoryAuthentication()
                .withUser("user").password("user").roles(UserRole.USER.name()).and()
                .withUser("admin").password("admin").roles(UserRole.ADMIN.name(), UserRole.USER.name());
    }
}
