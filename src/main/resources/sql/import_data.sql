INSERT INTO PUBLIC.USERS(ID, USERNAME, PASSWORD) VALUES
(1, 'jackson', 'jack.one'),
(2, 'douglas', 'doug.two');

INSERT INTO PUBLIC.ORDERS(ID, ITEMS, CREATION_DATE, USER_ID) VALUES
(1, 'breed, milk, coffe', '2019/03/04 11:20', 1),
(2, 'cake, chocolate, sweets', '2019/03/03 10:00', 1),
(3, 'cornflakes', '2019/03/02 09:14', 1),
(4, 'chicken pasta', '2019/03/01 15:00', 1),
(5, 'sushi', '2019/03/04 19:00', 2),
(6, 'pizza, beer', '2019/03/03 16:00', 2),
(7, 'sandwich', '2019/03/02 11:33', 2),
(8, 'black tea, muffin', '2019/03/01 18:27', 2);