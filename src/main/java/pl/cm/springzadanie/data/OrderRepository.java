package pl.cm.springzadanie.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.cm.springzadanie.model.OrderDetails;

@Repository
public interface OrderRepository extends JpaRepository<OrderDetails, Long> {

    OrderDetails findById(Long id);

}
