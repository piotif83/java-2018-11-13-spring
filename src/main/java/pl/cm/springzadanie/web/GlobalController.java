package pl.cm.springzadanie.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class GlobalController {

    @GetMapping("/user")
    public String showUser() {
        return "user/view";
    }

    @GetMapping("/admin")
    public String showAdmin() {
        return "admin/view";
    }

    @GetMapping("/login")
    public String showLogin() {
        return "login";
    }

}
