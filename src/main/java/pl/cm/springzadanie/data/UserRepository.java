package pl.cm.springzadanie.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.cm.springzadanie.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
}
