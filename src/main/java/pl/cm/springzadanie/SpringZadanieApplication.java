package pl.cm.springzadanie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringZadanieApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringZadanieApplication.class, args);
	}

}
